#!/usr/bin/env python2.4

from distutils.core import setup

if __name__ == '__main__':
    setup(name='bzrirker',
      description='Irker plugin for Bazaar',
      keywords='plugin bzr irker',
      version='0.0.1',
      url='https://bitbucket.org/drnlm/bzr-irker-plugin',
      download_url='https://bitbucket.org/drnlm/bzr-irker-plugin/downloads',
      license='GPL',
      author='Neil Muller',
      author_email='drnlmuller+irker@gmail.com',
      long_description="""
      Hooks into Bazaar and sends commit notification messages via irker.
      """,
      package_dir={'bzrlib.plugins.bzrirker':'.',
                   'bzrlib.plugins.bzrirker.tests':'tests'},
      packages=['bzrlib.plugins.bzrirker',
                'bzrlib.plugins.bzrirker.tests']
      )
