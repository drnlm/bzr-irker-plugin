# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8 ai ts=4 sts=4 et sw=4
# Copyright 2012 Neil Muller
# GPL 2+ - see COPYING for details
"""Sending irker notifications for commits and branch changes.

This will talk to an irkerd (currently assumed to be running on localhost)
and post commit logs.

Useful configuration settings:
    irker_project - The name of the project used in announcements
    irker_channels - The irc channels to post announcements to
    irker_colours - Settings for using colours in the announcements (ANSI or
        mIRC)
    irker_server - Host for the irkerd server (localhost by default)
    irker_port - port to connect to (6659 by default)
"""

from bzrlib.config import option_registry
from bzrlib.lazy_import import lazy_import

# lazy_import so that it doesn't get loaded if it isn't used
# Approach borrowed from bzr-email
lazy_import(globals(), """\
from bzrlib.plugins.bzrirker import irkerhook as _irkerhook
""")


def branch_commit_hook(local, master, old_revno, old_revid,
        new_revno, new_revid):
    """This is the post_commit hook that runs after commit."""
    if local is None:
        _irkerhook.IrkerSender(master, new_revid,
                master.get_config_stack()).send()
    else:
        _irkerhook.IrkerSender(local, new_revid,
                master.get_config_stack()).send()


def branch_post_change_hook(params):
    """This is the post_change_branch_tip hook."""
    # (branch, old_revno, new_revno, old_revid, new_revid)
    br = params.branch
    revs = br._revision_history()[params.old_revno:params.new_revno]
    for rev_id in revs:
        _irkerhook.IrkerSender(br, rev_id, br.get_config_stack()).send()


def test_suite():
    from unittest import TestSuite
    import bzrlib.plugins.bzrirker.tests
    result = TestSuite()
    result.addTest(bzrlib.plugins.bzrirker.tests.test_suite())
    return result


option_registry.register_lazy("irker_channels",
    "bzrlib.plugins.bzrirker.irkerhook", "opt_irker_channels")
option_registry.register_lazy("irker_colours",
    "bzrlib.plugins.bzrirker.irkerhook", "opt_irker_colours")
option_registry.register_lazy("irker_project",
    "bzrlib.plugins.bzrirker.irkerhook", "opt_irker_project")
option_registry.register_lazy("irker_server",
    "bzrlib.plugins.bzrirker.irkerhook", "opt_irker_server")
option_registry.register_lazy("irker_port",
    "bzrlib.plugins.bzrirker.irkerhook", "opt_irker_port")

from bzrlib.hooks import install_lazy_named_hook
install_lazy_named_hook("bzrlib.branch", "Branch.hooks",
        'post_commit', branch_post_change_hook, 'bzr-irker')
